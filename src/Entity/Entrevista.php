<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntrevistaRepository")
 * @ORM\Table(name="entrevistas")
 */
class Entrevista {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombreEntrevistado;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    public function getId(): ?int {
        return $this->id;
    }

    public function getNombreEntrevistado(): ?string {
        return $this->nombreEntrevistado;
    }

    public function setNombreEntrevistado(string $nombreEntrevistado): self {
        $this->nombreEntrevistado = $nombreEntrevistado;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

}
